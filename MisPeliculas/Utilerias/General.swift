//
//  General.swift
//  MisPeliculas
//
//  Created by Fabian Fonseca Sandoval on 12/02/23.
//

import Foundation
import SVProgressHUD

public class General {
    
    static let apiKey = "445bd28037810adb75279186a725a799"
    static let baseUrl = "https://api.themoviedb.org/3/"
    static let languageApi = "es"
    
    static func iniciarProgressDialog() {
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setDefaultMaskType(.custom)
        SVProgressHUD.setForegroundColor(UIColor(named: "verde1")!)
        SVProgressHUD.setBackgroundLayerColor(UIColor(named: "gris")!.withAlphaComponent(0.1))
        SVProgressHUD.show(withStatus: "Cargando")
    }
    
    static func detenerProgressDialog() {
        SVProgressHUD.dismiss()
    }
    
    static func generarDialogo(titulo: String, mensaje: String, vc: UIViewController) {
        let alert = UIAlertController(title: titulo, message: mensaje, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    static func formatDateText(dateText: String!, dateFormaInput: String!, dateFormaOutput: String!) -> String! {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = dateFormaInput
        var date = dateFormater.date(from: dateText)
        dateFormater.dateFormat = dateFormaOutput
        var result = dateFormater.string(from: date!)
        return result
    }
}
