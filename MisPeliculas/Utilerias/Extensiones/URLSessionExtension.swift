//
//  URLSessionExtension.swift
//  MisPeliculas
//
//  Created by Fabian Fonseca Sandoval on 18/02/23.
//

import Foundation

struct MyUrls {
    //LOGIN
    static let authenticationRequestToken = URL(string: "\(General.baseUrl)authentication/token/new?api_key=\(General.apiKey)")
    static let authenticationWhitLogin = URL(string: "\(General.baseUrl)authentication/token/validate_with_login?api_key=\(General.apiKey)")
    static let authenticationNewSession = URL(string: "\(General.baseUrl)authentication/session/new?api_key=\(General.apiKey)")
}

extension URLSession {
    
    enum CustomError: Error {
        case invalidUrl
        case badRequest
        case invalidData
    }
    
    func customRequest<T: Codable>(url: URL?,
                                   method: String?,
                                   body: [String: Any]?,
                                   responsType: T.Type,
                                   completion: @escaping (Result<T, Error>) -> Void) {
        
        guard let url = url else {
            completion(.failure(CustomError.invalidUrl))
            return
        }
        
        guard let method = method else {
            completion(.failure(CustomError.invalidUrl))
            return
        }
        
        var requestBody: Data? = nil
        
        if let json = body {
            do {
                requestBody = try JSONSerialization.data(withJSONObject: json)
            } catch {
                completion(.failure(CustomError.badRequest))
                return
            }
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method
        request.httpBody = requestBody
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = self.dataTask(with: request) { data, response, error in
            
            let httpResponse = response as! HTTPURLResponse
            
            if httpResponse.statusCode != 200 {
                do {
                    let iniciarSesionError = try JSONDecoder().decode(ErrorGenericoApi.self, from: data!)
                    completion(.failure(iniciarSesionError))
                } catch {
                    completion(.failure(error))
                }
                return
            }
            
            guard let data = data else {
                if let error = error {
                    completion(.failure(error))
                } else {
                    completion(.failure(CustomError.invalidData))
                }
                return
            }
            
            do {
                let result = try JSONDecoder().decode(responsType, from: data)
                completion(.success(result))
            } catch {
                completion(.failure(error))
            }
        }
        
        task.resume()
    }
}
