//
//  Entity.swift
//  MisPeliculas
//
//  Created by Fabian Fonseca Sandoval on 12/02/23.
//

import Foundation

struct RequestTokenSuccess: Codable {
    var success: Bool
    var expires_at: String
    var request_token: String
}

struct IniciarSesionSuccess: Codable {
    var username: String
    var password: String
    var request_token: String
}

struct ErrorGenericoApi: Codable, Error {
    var success: Bool
    var status_code: Int
    var status_message: String
}
