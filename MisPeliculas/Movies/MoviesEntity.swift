//
//  MoviesEntity.swift
//  MisPeliculas
//
//  Created by Fabian Fonseca Sandoval on 13/02/23.
//

import Foundation

//MOVIES
struct MovieResponse: Codable {
    var page: Int
    var results: [Movie]
    var total_pages: Int
    var total_results: Int
}

struct Movie: Codable {
    var adult: Bool?
    var backdrop_path: String?
    var id: Int?
    var original_language: String?
    var original_title: String?
    var overview: String?
    var popularity: Double?
    var poster_path: String?
    var release_date: String?
    var title: String?
    var video: Bool?
    var vote_average: Double?
    var vote_count: Int?
}

//TV

struct TvResponse: Codable {
    var page: Int
    var results: [TvProgram]
    var total_pages: Int
    var total_results: Int
}

struct TvProgram: Codable {
    var backdrop_path: String?
    var first_air_date: String?
    var id: Int?
    var name: String?
    var original_language: String?
    var original_name: String?
    var overview: String?
    var popularity: Double?
    var poster_path: String?
    var vote_average: Double?
    var vote_count: Int?
}
