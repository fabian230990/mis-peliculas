//
//  MoviewCollectionViewCell.swift
//  MisPeliculas
//
//  Created by Fabian Fonseca Sandoval on 13/02/23.
//

import UIKit

class MoviewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var contenedorPrincipal: UIView!
    @IBOutlet weak var contenedorImagen: UIView!
    @IBOutlet weak var imagenPelicula: UIImageView!
    
    @IBOutlet weak var nombrePeliculaLbl: UILabel!
    @IBOutlet weak var fechaLanzamientoLbl: UILabel!
    @IBOutlet weak var puntagePeliculaLbl: UILabel!
    @IBOutlet weak var descripcionPelicilaLbl: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        initCell()
    }
    
    func initCell() {
        self.contenedorPrincipal.layer.cornerRadius = 30.0
        self.contenedorImagen.layer.cornerRadius = 30.0
        
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = .zero
        layer.shadowRadius = 5.0
        layer.shadowOpacity = 0.7
        layer.masksToBounds = false
        
        self.contenedorImagen.layer.shadowColor = UIColor.black.cgColor
        self.contenedorImagen.layer.shadowOffset = .zero
        self.contenedorImagen.layer.shadowRadius = 5.0
        self.contenedorImagen.layer.shadowOpacity = 0.8
        self.contenedorImagen.layer.masksToBounds = false
        
        self.imagenPelicula.layer.cornerRadius = 30.0
    }
    
    func configCell(object: Any, type: Int) {
        switch type {
        case 1, 2:
            if let movie = object as? Movie {
                if let imagePath = movie.poster_path {
                    self.imagenPelicula.downloaded(from: "https://image.tmdb.org/t/p/w500/\(imagePath)")
                } else {
                    self.imagenPelicula.image = UIImage(named: "no_imagen_disponible")
                }
                
                self.nombrePeliculaLbl.text = movie.title
                self.fechaLanzamientoLbl.text = General.formatDateText(dateText: movie.release_date, dateFormaInput: "yyyy-MM-dd", dateFormaOutput: "MMM dd',' yyyy")
                
                if let vote_average = movie.vote_average {
                    self.puntagePeliculaLbl.text = "★ \(vote_average)"
                }
                
                self.descripcionPelicilaLbl.text = movie.overview
                
                if type == 2 {
                    self.contenedorPrincipal.backgroundColor = UIColor(named: "azul")
                }
            }
            break
        case 3, 4:
            if let tvProgram = object as? TvProgram {
                if let imagePath = tvProgram.poster_path {
                    self.imagenPelicula.downloaded(from: "https://image.tmdb.org/t/p/w500/\(imagePath)")
                } else {
                    self.imagenPelicula.image = UIImage(named: "no_imagen_disponible")
                }
                
                self.nombrePeliculaLbl.text = tvProgram.name
                
                if let first_air_date = tvProgram.first_air_date, !first_air_date.isEmpty {
                    self.fechaLanzamientoLbl.text = General.formatDateText(dateText: first_air_date, dateFormaInput: "yyyy-MM-dd", dateFormaOutput: "MMM dd',' yyyy")
                } else {
                    self.fechaLanzamientoLbl.text = "---"
                }
                
                
                if let vote_average = tvProgram.vote_average {
                    self.puntagePeliculaLbl.text = "★ \(vote_average)"
                }
                
                if let overview = tvProgram.overview, !overview.isEmpty {
                    self.descripcionPelicilaLbl.text = overview
                } else {
                    self.descripcionPelicilaLbl.text = "..."
                }
                
                if type == 3 {
                    self.contenedorPrincipal.backgroundColor = UIColor(named: "guinda")
                } else {
                    self.contenedorPrincipal.backgroundColor = UIColor(named: "gris")
                }
                
            }
            break
        default:
            debugPrint("No coincidencia en celada")
        }
    }

}
