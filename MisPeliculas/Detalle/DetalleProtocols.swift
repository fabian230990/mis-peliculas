//
//  DetalleProtocols.swift
//  MisPeliculas
//
//  Created Fabian Fonseca Sandoval on 13/02/23.
//  Copyright © 2023 ___ORGANIZATIONNAME___. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import Foundation

//MARK: Wireframe -
protocol DetalleWireframeProtocol: class {

}
//MARK: Presenter -
protocol DetallePresenterProtocol: class {

}

//MARK: Interactor -
protocol DetalleInteractorProtocol: class {

  var presenter: DetallePresenterProtocol?  { get set }
}

//MARK: View -
protocol DetalleViewProtocol: class {

  var presenter: DetallePresenterProtocol?  { get set }
}
