//
//  PerfilViewController.swift
//  MisPeliculas
//
//  Created Fabian Fonseca Sandoval on 13/02/23.
//  Copyright © 2023 ___ORGANIZATIONNAME___. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class PerfilViewController: UIViewController {
    
    @IBOutlet weak var imagenPerfil: UIImageView!
    @IBOutlet weak var nombreLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var idLbl: UILabel!
    @IBOutlet weak var favoritasCollectionView: UICollectionView!
    
    var presenter: PerfilPresenterProtocol?
    
    var perfilResponse: PerfilResponse?
    
    var listaFavoritas: [Movie] = []
    

	override func viewDidLoad() {
        super.viewDidLoad()
        setGradientBackground()
        self.configView()
        self.configureCollectionViews()
        
        self.presenter?.consultarPerfil()
        self.listaFavoritas = FavoriteMovieDB().consultarTodasFavoriteMovies()
        self.favoritasCollectionView.reloadData()
    }
    
    func configureCollectionViews() {
        let bundle = Bundle(for: PerfilViewController.self)
        
        self.favoritasCollectionView.delegate = self
        self.favoritasCollectionView.dataSource = self
        self.favoritasCollectionView.register(UINib(nibName: "MoviewCollectionViewCell", bundle: bundle), forCellWithReuseIdentifier: "MoviewCollectionViewCell")
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor(named: "verde1")!.cgColor
        let colorBottom = UIColor.black.cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 0.35]
        gradientLayer.frame = self.view.bounds
                
        self.view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    func configView() {
        self.imagenPerfil.layer.cornerRadius = 50.0
    }
}

extension PerfilViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listaFavoritas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = self.favoritasCollectionView.dequeueReusableCell(withReuseIdentifier: "MoviewCollectionViewCell", for: indexPath) as? MoviewCollectionViewCell {
            
            cell.configCell(object: self.listaFavoritas[indexPath.row], type: 1)
            
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "Pelicula Favorita", message: "¿Quires quitar '\(self.listaFavoritas[indexPath.row].title)' de tus peliculas favoritas?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel))
        alert.addAction(UIAlertAction(title: "Si, quitar de favoritas", style: .destructive, handler: { action in
            _ = FavoriteMovieDB().eliminaFavoriteMovie(idEliminar: self.listaFavoritas[indexPath.row].id ?? 0)
        }))
        self.present(alert, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let ancho = self.view.bounds.width / 2 - 20
        return CGSize(width: ancho, height: 325.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
}

extension PerfilViewController: PerfilViewProtocol {
    
    func consultarPerfilSuccess(data: PerfilResponse) {
        
        DispatchQueue.main.async {
            self.perfilResponse = data
            
            if let imagePath = self.perfilResponse?.avatar.tmdb.avatar_path {
                self.imagenPerfil.downloaded(from: "https://image.tmdb.org/t/p/w500/\(imagePath)")
            } else {
                self.imagenPerfil.image = UIImage(named: "no_imagen_disponible")
            }
            
            self.nombreLbl.text = self.perfilResponse?.name
            self.userNameLbl.text = self.perfilResponse?.username
            self.idLbl.text = "\(self.perfilResponse?.id ?? 0)"
        }
        
    }
    
    func consultarPerfilError(error: String!) {
        print("Error: \(error ?? "")")
    }
    
}
