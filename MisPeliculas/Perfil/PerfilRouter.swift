//
//  PerfilRouter.swift
//  MisPeliculas
//
//  Created Fabian Fonseca Sandoval on 13/02/23.
//  Copyright © 2023 ___ORGANIZATIONNAME___. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class PerfilRouter: PerfilWireframeProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        // Change to get view from storyboard if not using progammatic UI
        let view = PerfilViewController(nibName: nil, bundle: nil)
        let interactor = PerfilInteractor()
        let router = PerfilRouter()
        let presenter = PerfilPresenter(interface: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        
        return view
    }
}
