//
//  PerfilEntity.swift
//  MisPeliculas
//
//  Created by Fabian Fonseca Sandoval on 13/02/23.
//

import Foundation

struct PerfilResponse: Codable {
    var avatar: PerfilAvatar
    var id: Int
    var name: String
    var username: String
}

struct PerfilAvatar: Codable {
    var tmdb: Avatar
}

struct Avatar: Codable {
    var avatar_path: String
}

struct PerfilSesion: Codable {
    var success: Bool
    var session_id: String
}
