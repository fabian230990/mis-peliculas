//
//  SesionDb.swift
//  MisPeliculas
//
//  Created by Fabian Fonseca Sandoval on 13/02/23.
//

import UIKit
import SQLite

public class SesionDB: LocalDataBase {
    
    let tablaSesion = Table("sesion")
    
    let username = Expression<String>("username")
    let password = Expression<String>("password")
    let requestToken = Expression<String>("request_token")
    
    func connectToDB() -> Bool {
        return self.connect()
    }
    
    func createTable() -> Bool {
        var response : Bool = false
        
        let createTable = self.tablaSesion.create(ifNotExists: true) { (table) in
            table.column(self.username, primaryKey: true)
            table.column(self.password)
            table.column(self.requestToken)
        }
        
        do {
            try self.connection.run(createTable)
            print("Session table was created")
            response = true
        } catch {
            print(error)
            response = false
        }
        return response
    }
    
    func dropTable() -> Bool {
        var response : Bool = false
        
        let dropTable = self.tablaSesion.drop(ifExists: true)
        
        do {
            try self.connection.run(dropTable)
            print("Session Table was droped")
            response = true
        } catch {
            print(error)
            response = false
        }
        
        return response
    }
    
    //--------------------DELETE--------------------
    
    func eliminaSesion() -> Bool {
        var response: Bool = false
        
        if self.connectToDB() {
            let eliminaSesion = self.tablaSesion.delete()
            
            do {
                try self.connection.run(eliminaSesion)
                print("TODAS LAS SESIONES FUERON ELIMINADAS")
                response = true
            } catch {
                print(error)
                response = false
            }
        }
        
        return response
    }
    
    //--------------------INSERT--------------------
    
    func insertaSesion(data: IniciarSesionSuccess) -> Bool {
        
        var response: Bool = false
        
        if self.connectToDB() {
            _ = self.eliminaSesion()
            
            let insertSesion = self.tablaSesion.insert(
                self.username <- data.username,
                self.password <- data.password,
                self.requestToken <- data.request_token
            )
            
            do {
                try self.connection.run(insertSesion)
                print("SESION INSERTADA")
                response = true
            } catch {
                print(error)
                response = false
            }
        }
        
        return response
    }
    
    //--------------------QUERY--------------------
    
    func listAll() -> Array<Row>{
        var sesiones : Array<Row>!
        do {
            sesiones = try Array(self.connection.prepare(self.tablaSesion))
        } catch {
            print(error)
        }
        return sesiones
    }
    
    //--------------------QUERY--------------------
    
    func validarSesionIniciada() -> Bool {
        if self.connectToDB() {
            let clientes = self.listAll()
            if clientes.count > 0 {
                return true
            }
        }
        return false
    }
    
    //--------------------QUERY--------------------
    
    func consultarSesion() -> IniciarSesionSuccess? {
        if self.connectToDB() {
            let sesionesDB = self.listAll()
            if sesionesDB.count > 0 {
                let sesionDB = sesionesDB[0]
                let sesion = IniciarSesionSuccess(username: sesionDB[self.username], password: sesionDB[self.password], request_token: sesionDB[self.requestToken])
                return sesion
            }
        }
        return nil
    }
}

