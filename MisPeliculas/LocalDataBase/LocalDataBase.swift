//
//  LocalDataBase.swift
//  MisPeliculas
//
//  Created by Fabian Fonseca Sandoval on 13/02/23.
//

import UIKit
import SQLite

public class LocalDataBase {
    var connection : Connection!
    
    func connect() -> Bool {
        var response : Bool = false
        do {
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentDirectory.appendingPathComponent("mis-peliculas").appendingPathExtension("sqlite3")
            let database = try Connection(fileUrl.path)
            self.connection = database
            print("DATABASE WAS CREATED")
            response = true
        } catch {
            print(error)
            response = false
        }
        return response
    }
}

extension Connection {
    public var userVersion: Int32 {
        get { return Int32(try! scalar("PRAGMA user_version") as! Int64)}
        set { try! run("PRAGMA user_version = \(newValue)") }
    }
}
