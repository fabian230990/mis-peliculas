//
//  FavoriteMovie.swift
//  MisPeliculas
//
//  Created by Fabian Fonseca Sandoval on 18/02/23.
//

import UIKit
import SQLite

public class FavoriteMovieDB: LocalDataBase {
    
    let tablaFavoriteMovie = Table("favorite_movie")
    
    let adult = Expression<Bool?>("adult")
    let backdrop_path = Expression<String?>("backdrop_path")
    let id = Expression<Int>("id")
    let original_language = Expression<String?>("original_language")
    let original_title = Expression<String?>("original_title")
    let overview =  Expression<String?>("overview")
    let popularity = Expression<Double?>("popularity")
    let poster_path = Expression<String?>("poster_path")
    let release_date = Expression<String?>("release_date")
    let title = Expression<String?>("title")
    let video = Expression<Bool?>("video")
    let vote_average = Expression<Double?>("vote_average")
    let vote_count = Expression<Int?>("vote_count")
    
    func connectToDB() -> Bool {
        return self.connect()
    }
    
    func createTable() -> Bool {
        var response : Bool = false
        
        let createTable = self.tablaFavoriteMovie.create(ifNotExists: true) { (table) in
            table.column(self.adult)
            table.column(self.backdrop_path)
            table.column(self.id, primaryKey: true)
            table.column(self.original_language)
            table.column(self.original_title)
            table.column(self.overview)
            table.column(self.popularity)
            table.column(self.poster_path)
            table.column(self.release_date)
            table.column(self.title)
            table.column(self.video)
            table.column(self.vote_average)
            table.column(self.vote_count)
        }
        
        do {
            try self.connection.run(createTable)
            print("Favorite movie table was created")
            response = true
        } catch {
            print(error)
            response = false
        }
        return response
    }
    
    func dropTable() -> Bool {
        var response : Bool = false
        
        let dropTable = self.tablaFavoriteMovie.drop(ifExists: true)
        
        do {
            try self.connection.run(dropTable)
            print("Favorite movie Table was droped")
            response = true
        } catch {
            print(error)
            response = false
        }
        
        return response
    }
    
    //--------------------DELETE--------------------
    
    func eliminaFavoriteMovie(idEliminar: Int) -> Bool {
        var response: Bool = false
        _ = self.connectToDB()
        let filter = self.tablaFavoriteMovie.filter(id == idEliminar)
        let eliminaFavoriteMovie = filter.delete()
        
        do {
            try self.connection.run(eliminaFavoriteMovie)
            print("FAVORITE MOVIE ELIMINADO WHERE id = \(idEliminar)")
            response = true
        } catch {
            print(error)
            response = false
        }
        return response
    }
    
    //--------------------QUERY--------------------
    
    func listAll() -> Array<Row>{
        var sesiones : Array<Row>!
        do {
            sesiones = try Array(self.connection.prepare(self.tablaFavoriteMovie))
        } catch {
            print(error)
        }
        return sesiones
    }
    
    //--------------------INSERT--------------------
    
    func insertaFavoriteMovie(movie: Movie) -> Bool {
        var response: Bool = false
        
        _ = self.connectToDB()
        let insertFM = self.tablaFavoriteMovie.insert(
            self.adult <- movie.adult,
            self.backdrop_path <- movie.backdrop_path,
            self.id <- movie.id!,
            self.original_language <- movie.original_language,
            self.original_title <- movie.original_title,
            self.overview <- movie.overview,
            self.popularity <- movie.popularity,
            self.poster_path <- movie.poster_path,
            self.release_date <- movie.release_date,
            self.title <- movie.title,
            self.video <- movie.video,
            self.vote_average <- movie.vote_average,
            self.vote_count <- movie.vote_count
        )
        
        do {
            try self.connection.run(insertFM)
            print("FM INSERTADO")
            response = true
        } catch {
            print(error)
            response = false
        }
        
        return response
    }
    
    //--------------------QUERY--------------------
    
    func consultarTodasFavoriteMovies() -> [Movie] {
        var listaMovies: [Movie] = []
        
        if self.connectToDB() {
            do {
                let query = self.tablaFavoriteMovie
                var movies = try Array(self.connection.prepare(query))
                for movie in movies {
                    var fm = Movie()
                    
                    fm.adult = movie[self.adult]
                    fm.backdrop_path = movie[self.backdrop_path]
                    fm.id = movie[self.id]
                    fm.original_language = movie[self.original_language]
                    fm.original_title = movie[self.original_title]
                    fm.overview = movie[self.overview]
                    fm.popularity = movie[self.popularity]
                    fm.poster_path = movie[self.poster_path]
                    fm.release_date = movie[self.release_date]
                    fm.title = movie[self.title]
                    fm.video = movie[self.video]
                    fm.vote_average = movie[self.vote_average]
                    fm.vote_count = movie[self.vote_count]
                    
                    listaMovies.append(fm)
                }
            } catch {
                print(error)
            }
        }
        
        return listaMovies
    }
    
    func consultarFavoriteMoviePorId(idConsulta: Int) -> Movie? {
        if self.connectToDB() {
            do {
                let query = self.tablaFavoriteMovie.filter(id == idConsulta)
                let movies = try Array(self.connection.prepare(query))
                if movies.count > 0 {
                    let movie = movies[0]
                    var fm = Movie()
                    
                    fm.adult = movie[self.adult]
                    fm.backdrop_path = movie[self.backdrop_path]
                    fm.id = movie[self.id]
                    fm.original_language = movie[self.original_language]
                    fm.original_title = movie[self.original_title]
                    fm.overview = movie[self.overview]
                    fm.popularity = movie[self.popularity]
                    fm.poster_path = movie[self.poster_path]
                    fm.release_date = movie[self.release_date]
                    fm.title = movie[self.title]
                    fm.video = movie[self.video]
                    fm.vote_average = movie[self.vote_average]
                    fm.vote_count = movie[self.vote_count]
                }
                for movie in movies {
                    var fm = Movie()
                    
                    fm.adult = movie[self.adult]
                    fm.backdrop_path = movie[self.backdrop_path]
                    fm.id = movie[self.id]
                    fm.original_language = movie[self.original_language]
                    fm.original_title = movie[self.original_title]
                    fm.overview = movie[self.overview]
                    fm.popularity = movie[self.popularity]
                    fm.poster_path = movie[self.poster_path]
                    fm.release_date = movie[self.release_date]
                    fm.title = movie[self.title]
                    fm.video = movie[self.video]
                    fm.vote_average = movie[self.vote_average]
                    fm.vote_count = movie[self.vote_count]
                    
                    return fm
                }
            } catch {
                print(error)
            }
        }
        
        return nil
    }
}
