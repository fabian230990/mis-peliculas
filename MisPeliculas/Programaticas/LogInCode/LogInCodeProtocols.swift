//
//  LogInCodeProtocols.swift
//  MisPeliculas
//
//  Created Fabian Fonseca Sandoval on 18/02/23.
//  Copyright © 2023 ___ORGANIZATIONNAME___. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import Foundation

//MARK: Wireframe -
protocol LogInCodeWireframeProtocol: AnyObject {

}
//MARK: Presenter -
protocol LogInCodePresenterProtocol: AnyObject {

    func iniciarSesion(userName: String!, password: String!)
    func iniciarSesionSuccess(data: IniciarSesionSuccess)
    func iniciarSesionError(error: String!)
    
}

//MARK: Interactor -
protocol LogInCodeInteractorProtocol: AnyObject {

  var presenter: LogInCodePresenterProtocol?  { get set }
    
    func iniciarSesion(userName: String!, password: String!)
}

//MARK: View -
protocol LogInCodeViewProtocol: AnyObject {

  var presenter: LogInCodePresenterProtocol?  { get set }
    
    func iniciarSesionSuccess(data: IniciarSesionSuccess)
    func iniciarSesionError(error: String!)
}
