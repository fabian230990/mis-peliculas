//
//  AppDelegate.swift
//  MisPeliculas
//
//  Created by Fabian Fonseca Sandoval on 11/02/23.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //HABILITAR TECLADO AGIL
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Listo"
        IQKeyboardManager.shared.toolbarTintColor = UIColor(named: "verde1")
        
        //CREACION DE LA BASE DE DATOS
        initLocalDataBase()
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func initLocalDataBase() {
        //TABLA SESION
        let tablaSesion = SesionDB()
        if tablaSesion.connectToDB() {
            _ = tablaSesion.createTable()
        }
        //TABLA FAVORITE MOVIES
        let tablaFavoriteMovie = FavoriteMovieDB()
        if tablaFavoriteMovie.connectToDB() {
            _ = tablaFavoriteMovie.createTable()
        }
    }

}

